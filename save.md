> import shelve

`shelve` will store a program's data.

Start by making a file:

shelfFile = shelve.open('someData')

Check its file-type:

> type(shelfFile)

`<class 'shelve.DbfilenameShelf'>`

Make some data:

> myCats = ['Sita', 'Annie', 'Socks']

Then put the data into the `shelfFile` database:

> shelfFile['cats'] = myCats

And write the file:

shelfFile.close()

If you want to look at the data, use:

> shelfFile['myCats']

## Save as Text

### Pretty Printing

> import pprint

> pprint.pprint(myCats)

