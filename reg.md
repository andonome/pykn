Import the regex module:

> import re

| Type              | Notation   |
|:------------------|:-----------|
| Digit             | `\d`       |
| Three digits      | `\d{3}`    |
| alphanum          | `\w`       |
| non-alphanum      | `\W`       |
| space,tab,newline | `\s`       |
| not-spaces        | `\S`       |
| lowercase vowels  | `[aeiou]`  |
| not vowels        | `[^aeiou]` |
| starts with X     | `^X`       |
| ends with X       | `X$`       |
| wildcard          | `.`        |
| greedy match      | `.*`       |
| zero or more X    | `X?`       |
| XXX               | `X{3}      |
| 3 or more X       | `X{3,}     |
| up to 4 X         | `X{,4}     |
| 3-5 X             | `X{3,5}    |
|                   |            |
|                   |            |
|                   |            |
|                   |            |

# Creation

Make a regular expression object with `re.compile` 

## Regex Groups

Find a Glasgow phone number, and split the area code from the rest:

## Pipe your 'Or'

Compile a regex to detect 'Batman' or 'Tina Fey':

> heroRegex = re.compile(r'Batman|Tina Fey')

mo = heroRegex.search('Tina Fey is Batman.')

This returns `Tina Fey`, because it's the first matching string.

# Examples

## Robocop

Search for 'robocop' while ignoring case:

> robocopReg = re.compile(r'robocop', re.I)

## UK Number

Match a UK phone number, like '01415103482'.

> (00|\+)44(1|2)(\s?\n{3}\s?\n{3\s?\n{3}})

Spread it over multiple lines like this:

> phoneRegex = re.compile('''(
> (00|0|\+) # a '+' or '00'
> 44
> \s?|-?|\.?\n{3} # a possible separator, then three digits.
> \s?|-?|\.?\n{3} # a possible separator, then three digits.
> \s?|-?|\.?\n{3} # a possible separator, then three digits.
> )''')

