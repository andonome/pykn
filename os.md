# Clipboard Interaction

Install the pyperclip module with:

> pip3 install pyperclip

Copy:

> pyperclip.copy('some text')

> pyperclip.paste()

# `os`

To work with most import functions, you need to `import os`.

## Paths
Make a path which works on any filesystem:

> os.path.join('usr', 'bin', 'spam')

On Unix:

`usr/bin/spam`

On DOS:

`usr\\bin\\spam`

### Create a List of Filenames

> myFiles = ['accounts.txt', 'details.csv', 'invite.odt']

> for filename in myFiles:
> 	print(os.path.join('home', 'ghost', 'project-1', filename))

```
/home/ghost/project-1/accounts.txt
/home/ghost/project-1/details.csv
/home/ghost/project-1/invite.odt

```

### Current Working Directory

Get the cwd:

> os.getcwd()

Change the cwd:

> os.chdir()

Get an absolute path:

> os.path.abspath(file.txt)

`/home/ghost/Projects/project-1/file.txt`

Make a directory path:

> os.makedirs('/home/ghost/Projects/project-1/subdir')

This works like `mkdir -p project-1/subdir`.

Check if something's an absolute path:

> os.path.isabs('/home/ghost')

`True`

Print something as an absolute path:

> os.path.abspath('.')

`'/home/ghost'`

Split up a file's path into the path and the file:

> filePath = os.getcwd() + '/eight.py'

> print(filePath)

`/home/ghost/Projects/python/eight.py`

> os.path.split(filePath)

`('/home/ghost/Projects/python', 'eight.py')`

Get size of a file:

> os.path.getsize('/home/ghost/file.txt')

`234` (meaing 234 bytes)

List files in a directory:

> os.listdir('/home/ghost/Projects')

You can also check for existence with:

> os.path.exists

> os.path.isfile

> os.path.isdir

## Open Files

> open('memify.py')

`<_io.TextIOWrapper name='memify.py' mode='r' encoding='UTF-8'>`

The default is `open('memify.py','r')`, meaning 'read-only mode'.

You can make this an object, which can be read:

> myFile = open('memify.py')

Then read with:

> myFile.read()

Or get each line of the file as strings:

> myFile.readlines()


## Globbing

> from pathlib import Path

Define a path:

> p = Path('/home/ghost/Projects')

Glob all files in the path:

> p.glob('\*')

List all text files:

> list(p.glob('\*txt')

