# Numbers

> 1 + 1

`2`

Remainder/ Modulus

> 10 % 4

`2`

[numbers](basics/num.md)

# Strings

Print a string:

> "ni"

`ni`

Repeat/ multiply a string:

> "ni" \* 5

`ninininini`

Add strings:

> 'hello' + "world!"

`hello world!`

You cannot add a string to a number such as an integer, so to add a string to a number, turn the number into a string:

> x = 3

> "And the number of the counting shall be " + str(x)

`And the number of the counting shall be 3`

[strings](basics/str.md)

# Flow

Conditionals with if:

> if 3 > 1:
>	print("Maths has not yet broken")

and else:

> if x < 3:
> 	break
> elif x > 3:
> 	print("Three, my lord!")
> else:
> 	print("Throw!")

# Functions

0-argument function:

> def roll2D6():

>	x = random.randint(1, 6) + random.randint(1,6)

>	return x

> roll2D6()

`7`

1-argument function:

> def double(x):

>	x = 2\*x

>	return x

> double(4)

`8`

> double(roll2D6())

`16`

# Trying Errors

You can try to do something, and if that returns an error, do something else:

> try:
> 	return 42 / x
> except ZeroDivisionError:
> 	print("Try an even number")
> 

Inputing 'x = 2' returns `21.0`, while inputing 'x = 1' returns `Try an even number`.

# Lists

Make a list of strings:

> theNine = [ "frodo", "pipin", "merry", "aragorn", "gandalf", "legolas", "gimli" ]

Get the length of the list:

> len(theNine)

`8`

The list is missing one, so we can append it with Boromir:

> theNine.append('samwise')

Of course, Boromir dies later, so we have to remove him with:

> theNine.remove('boromir')

Later, golumn joins the group:

> theNine.append

[lists](basics/lists.md)

# Tuples

Tuples work like lists, except they're immutable:

> races = ('Dwarves', 'Elves', 'Men')

# Dictionaries

> racialSpecializations = {'Elves': 'bows', 'Dwarves': 'axes', 'Humans': 'dying'}

Fetch back they keys:

> racialSpecializations.keys()

Or values:

> racialSpecializations.values()

Add to a dictionary:

> racialSpecializations['Goblins'] = 'rocks'

