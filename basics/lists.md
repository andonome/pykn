# Sorting

Sort list 'theNine' alphabetically with:

> theNine.sort()

Make it reverse order with:

> theNine.sort(reverse=True)

## Reverse

Reverse list 'theNine':

> theNine.reverse()

