Make string upper case:

> "clip".upper

`CLIP`

Lower case:

> 'cLoP'.lower

`clop`

# Strings as Lists

'clip'[0]

`c`

Test is the character 'C' appears in 'clip':

> 'c' in 'clip'

`True`

Loop through a name, performing an operation:

> x = 1
> for i in 'AAARGH':
> 	print('#' * x + i)
> 	x+=1

## Global Variables

> global x

> 	x = 1

# Multiline Strings

# Literal Strings

You can print a raw ('literal') string with:

> print(r'I like the \n command')

`I like the \n command`

# Multiline Strings

> print('''Good multiline strings
> 
> Remove print statements from code
> 
> How else to haiku?''')

# Designating Variables with %

Use '%s' to specify strings inside strings, then specify which sub-strings after:

> name = 'Thror'
> dad = 'Thrain'
> "I am %s, son of %s, King under the Mountain!" %(name,dad)

# String Tests

Test for letters with `isalphra(x)`.
Test for letters and numbers with `isalnum(x)`.

# Testing Start and End

'Thror'.startswith('Thr')

`True`

> 'Bilbo Baggins'.endswith('Baggins')

True

# Joining Strings

> ' '.join(theNine)

`'frodo aragorn pippin merry legolas boromir'`

# Copy and Paste

Install `pyperclip` with pip.

> sudo pip install pyperclip

Copy text to clipboard:

> pyperclip.copy("theme")

> pyperclip.paste()

`theme`

